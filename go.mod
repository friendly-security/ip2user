module gitlab.com/friendly-security/ip2user

go 1.19

require (
	github.com/aws/aws-sdk-go v1.44.110 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/securego/gosec v0.0.0-20200401082031-e946c8c39989 // indirect
	github.com/spf13/cobra v1.5.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/tools v0.0.0-20200331202046-9d5940d49312 // indirect
)
