# Ip2User

A simple utility for those of us that use AWS cognito and ask themselves the question.
'This IP I am seeing in my logs, which user does that belong to?'

Of course it is not exact and there are a lot of caveats to this (and attribution in general),
which I might document here later.
