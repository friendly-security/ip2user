package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/spf13/cobra"
)

// TODO clean up local variable usage

func main() {
	rootCmd.Execute() // #nosec - errors in main are allowed to bubble up in this case
}

func init() {
	rootCmd.AddCommand(findCmd)
	rootCmd.AddCommand(listCmd)
	rootCmd.AddCommand(clearCacheCmd)
	rootCmd.PersistentFlags().IntVar(&numWorkers, "workers", 5, "How many workers to use when querying for user login histories.")
	rootCmd.PersistentFlags().StringVar(&userPoolId, "userPoolId", "", "The id of the UserPool to check")
	rootCmd.PersistentFlags().BoolVar(&noCache, "noCache", false, "If set the data will be freshly retrieved from AWS")

	findCmd.Flags().StringVar(&ipAddress, "ip", "", "The IP to look for")
}

var userPoolId string
var ipAddress string
var numWorkers int
var noCache bool

var rootCmd = &cobra.Command{
	Use:   "ip2user",
	Short: "Prints a list of cognito users that signed in with a given IP.",
	Long: `This program will build a list of IPs from which a user has logged for every
user in your Cognito UserPool and print those that logged in from the IP you are
looking for.`,
}

var findCmd = &cobra.Command{
	Use:   "find",
	Short: "Perform a search for users of a given IP",
	Run:   findUsersForIp,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if userPoolId == "" {
			return errors.New("userPoolId must be provided")
		}
		if ipAddress == "" {
			return errors.New("ip address to look for must be provided")
		}
		return nil
	},
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List login events for all users ",
	Run:   listLoginEvents,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if userPoolId == "" {
			return errors.New("userPoolId must be provided")
		}
		return nil
	},
}

var clearCacheCmd = &cobra.Command{
	Use:   "clear-cache",
	Short: "Clears the local cache",
	Run:   clearCache,
}

func findUsersForIp(cmd *cobra.Command, args []string) {

	loginHistory := loadLoginEvents()
	results := findUsersInLoginEvents(loginHistory)

	jsonRes, _ := json.MarshalIndent(results, "", "\t")
	fmt.Println(string(jsonRes))
}

func listLoginEvents(cmd *cobra.Command, args []string) {
	loginHistory := loadLoginEvents()

	jsonRes, _ := json.MarshalIndent(loginHistory, "", "\t")
	fmt.Println(string(jsonRes))
}

func clearCache(cmd *cobra.Command, args []string) {
	deleteCache()
}

type LoginHistory map[string][]string

func findUsersInLoginEvents(loginHistory LoginHistory) []string {
	results := []string{}

	for user, ips := range loginHistory {
		for _, ip := range ips {
			if ip == ipAddress {
				results = append(results, user)
				break
			}
		}
	}

	return results
}

type IpLookupResult struct {
	email string
	ips   []string
}
type LoginHistoryCache struct {
	CreatedAt    time.Time
	LoginHistory LoginHistory
}

func (l LoginHistoryCache) expired() bool {
	cacheValidity := time.Minute * 30
	return l.CreatedAt.Before(time.Now().Add(-cacheValidity))
}

func loadLoginEvents() LoginHistory {
	loginHistory := make(LoginHistory)

	if !noCache {
		cachedLoginHistory, err := loadLoginHistoryFromCache()
		if err == nil && !cachedLoginHistory.expired() {
			fmt.Fprintf(os.Stderr, "Using local cache\n")
			return cachedLoginHistory.LoginHistory
		}
	}

	cgn := cognitoidentityprovider.New(session.Must(session.NewSession()))

	users := getUsers(userPoolId, cgn)

	fmt.Fprintf(os.Stderr, "Getting Login events")

	// retrieve user login history in parallel
	//* This is not ideal as it will only start on a new batch, once the whole old batch
	//* is finished. If one of the users has a significantly longer login history, when
	//* compared to the other users, the next batch will only be queued once that one
	//* users history has been loaded. Still a massive improvement over no parallelism
	//* at all

	jobs := make(chan *cognitoidentityprovider.UserType)
	results := make(chan IpLookupResult)

	for w := 1; w <= numWorkers; w++ {
		go loginIpsWorker(w, cgn, jobs, results)
	}

	// TODO write tests for this

	for i := 0; i < len(users); i += numWorkers {
		j := i + numWorkers
		if j > len(users) {
			j = len(users)
		}

		for _, user := range users[i:j] {
			jobs <- user
		}

		for a := 0; a < len(users[i:j]); a++ {
			res := <-results
			loginHistory[res.email] = res.ips
		}
	}

	fmt.Fprintf(os.Stderr, "\n")

	writeLoginHistoryToCache(loginHistory)
	return loginHistory
}

func getCacheDir() string {
	home, err := os.UserHomeDir()
	cobra.CheckErr(err)
	cacheDir := filepath.Join(home, ".ip2user")
	return cacheDir
}

func loadLoginHistoryFromCache() (LoginHistoryCache, error) {
	cacheDir := getCacheDir()
	cachePath := filepath.Join(cacheDir, userPoolId+".json")

	jsonContent, err := ioutil.ReadFile(cachePath)
	if err != nil {
		return LoginHistoryCache{}, err
	}

	var l LoginHistoryCache
	err = json.Unmarshal(jsonContent, &l)
	cobra.CheckErr(err)

	return l, nil
}

func writeLoginHistoryToCache(l LoginHistory) {
	cacheDir := getCacheDir()
	cachePath := filepath.Join(cacheDir, userPoolId+".json")

	err := os.MkdirAll(cacheDir, os.ModePerm)
	cobra.CheckErr(err)

	content := LoginHistoryCache{CreatedAt: time.Now(), LoginHistory: l}

	jsonContent, err := json.Marshal(content)
	cobra.CheckErr(err)

	err = ioutil.WriteFile(cachePath, jsonContent, 0644)
	cobra.CheckErr(err)
}

func deleteCache() {
	cacheDir := getCacheDir()

	err := os.RemoveAll(cacheDir)
	cobra.CheckErr(err)
}

func loginIpsWorker(id int, cgn *cognitoidentityprovider.CognitoIdentityProvider, jobs <-chan *cognitoidentityprovider.UserType, results chan<- IpLookupResult) {
	for user := range jobs {
		fmt.Fprintf(os.Stderr, ".")
		email, err := extractEmailAttribute(user)
		cobra.CheckErr(err)

		authEvents := getLoginIps(*user.Username, userPoolId, cgn)

		ips := []string{}
		for _, event := range authEvents {
			ips = append(ips, *event.EventContextData.IpAddress)
		}
		results <- IpLookupResult{email: email, ips: ips}
	}
}

func getUsers(userPoolId string, cgn *cognitoidentityprovider.CognitoIdentityProvider) []*cognitoidentityprovider.UserType {
	fmt.Fprintf(os.Stderr, "Getting Users.")

	var users []*cognitoidentityprovider.UserType

	res, err := cgn.ListUsers(&cognitoidentityprovider.ListUsersInput{UserPoolId: &userPoolId})

	cobra.CheckErr(err)
	users = append(users, res.Users...)
	pt := res.PaginationToken

	for {
		if pt == nil {
			fmt.Fprintf(os.Stderr, "\n")
			break
		}
		fmt.Fprintf(os.Stderr, ".")
		res, err := cgn.ListUsers(&cognitoidentityprovider.ListUsersInput{UserPoolId: &userPoolId, PaginationToken: pt})
		cobra.CheckErr(err)
		users = append(users, res.Users...)
		pt = res.PaginationToken
	}

	return users
}

func getLoginIps(userName string, userpoolId string, cgn *cognitoidentityprovider.CognitoIdentityProvider) []*cognitoidentityprovider.AuthEventType {

	var authEvents []*cognitoidentityprovider.AuthEventType

	res, err := cgn.AdminListUserAuthEvents(&cognitoidentityprovider.AdminListUserAuthEventsInput{UserPoolId: &userpoolId, Username: &userName})
	cobra.CheckErr(err)

	authEvents = append(authEvents, res.AuthEvents...)
	pt := res.NextToken

	for {
		if pt == nil {
			break
		}
		fmt.Fprintf(os.Stderr, "/")
		res, err := cgn.AdminListUserAuthEvents(&cognitoidentityprovider.AdminListUserAuthEventsInput{UserPoolId: &userpoolId, Username: &userName, NextToken: pt})
		cobra.CheckErr(err)
		authEvents = append(authEvents, res.AuthEvents...)
		pt = res.NextToken
	}

	return authEvents
}

func extractEmailAttribute(u *cognitoidentityprovider.UserType) (string, error) {
	for _, attribute := range u.Attributes {
		if *attribute.Name == "email" {
			return *attribute.Value, nil
		}
	}
	return "", errors.New("user has no email attribute")
}
